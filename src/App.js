import React from 'react';
import Header from './components/Header/Header';
import Carousel from './components/Carousel/Carousel';

import './App.css';

function App() {
  return (
    <div className="app">
      <Header/>
      <Carousel/>
    </div>
  );
}

export default App;
