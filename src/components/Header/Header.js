import React from 'react';
import Brand from '../Brand/Brand';

import './Header.css';

export default function () {
    return (
        <header className="header">
            <Brand/>
            <button className="header__button">Открыть счёт</button>
            <a className="header__phone" href="tel:88005005545">8 800 600 55 45</a>
            <h1 className="header__title">Финансовые услуги на рынке ценных бумаг для компаний</h1>
        </header>
    );
}