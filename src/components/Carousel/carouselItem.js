import React from 'react';
import Hero from '../Hero/Hero';

export default function (title, subtitle, listItems, index, slideItems) {
    return (
        <li className="carousel__list-item" data-slide={ (index + 1) } key={index}>
            <Hero title={ title } subtitle={ subtitle } listItems={ listItems } image={ 'banner-' + (index + 1) }/>
            <div className="carousel__list-item-buttons">
                { slideItems.map((item, itemIndex) => <div className="carousel__list-item-button" key={itemIndex}></div>) }
            </div>
        </li>
    );
}