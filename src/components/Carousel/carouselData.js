export default [
    {
        title: 'Валютные операции по&nbsp;выгодному курсу',
        subtitle: 'На&nbsp;московской бирже без посредника',
        listItems: [
            '<strong>Комиссия от&nbsp;1&nbsp;копейки*</strong>',
            'Для заключения сделки достаточно иметь на&nbsp;брокерском счету лишь часть суммы',
            'Быстрый вывод валюты на&nbsp;ваш расчётный счёт',
        ]
    }, {
        title: 'Вложение свободных средств для юридических лиц',
        subtitle: 'Зарабатывайте с&nbsp;помощью однодневных инструментов московской биржи',
        navTitle: 'Однодневные инструменты&nbsp;МосБиржы',
        listItems: [
            'Срок от&nbsp;1&nbsp;дня',
            'Конкурентные ставки',
            'Надёжный контрагент<br>НКО &laquo;Национальный Клиринговый Центр&raquo; АО',
        ]
    }, {
        title: 'Инвестиции в&nbsp;еврооблигации и&nbsp;ETF',
        subtitle: 'Вкладывайте валюту в&nbsp;инструменты с&nbsp;фиксированной доходностью',
        listItems: [
            'Еврооблигации&nbsp;&mdash; ценные бумаги, номинированные в&nbsp;валюте с&nbsp;фиксированной доходностью',
            'ETF&nbsp;&mdash; группа инструменов для реализации инвестиционной стратегии, не&nbsp;требующей активного участия',
        ]
    }, {
        title: 'Брокерское обслуживание',
        subtitle: 'Получите доступ к&nbsp;российским и&nbsp;международным торговым площадкам:',
        listItems: [
            'А&nbsp;мы&nbsp;поможем составить портфель из&nbsp;ценных бумаг, валюты, фьючерсов и&nbsp;опционов',
            'Предоставим программное обеспечение, аналитику, обзоры рынков',
        ]
    }, {
        title: 'Привлечение финансирования',
        subtitle: 'Брокер предоставляет вам возможность вывода денежных средств под обеспечение в&nbsp;виде валюты или ценных бумаг',
        listItems: [
            'Для решения кассовых разрывов, развития бизнеса и&nbsp;других целей',
            'Низкие процентные ставки',
            'Гибкие сроки возврата',
        ]
    }, {
        title: 'Снижение валютных и&nbsp;рыночных рисков',
        subtitle: '',
        listItems: [
            'Фиксируйте курс сегодня для будущих расчётов',
            'Проконсультируем, дадим экспертную оценку, порекомендуем оптимальное решение для защиты от&nbsp;валютных колебаний',
        ]
    }
];