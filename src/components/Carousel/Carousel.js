import React from 'react';
import carouselItem from './carouselItem';
import slideItems from './carouselData';

import './Carousel.css';

class Carousel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            slide: 1,
            slideItems,
        };
        
        this.carousel = React.createRef();
        this.lastStatusMobile = !window.matchMedia('screen and (min-width: 1024px)').matches;
        window.addEventListener('resize', this.handleResize.bind(this));
    }

    handleResize() {
        if (window.matchMedia('screen and (min-width: 1024px)').matches) {
            if (this.lastStatusMobile && this.carousel.current) {
                this.setState({slide: 1});
                this.carousel.current.scrollLeft = 0;
                this.lastStatusMobile = false;
            }
        } else {
            this.lastStatusMobile = true;
        }
    }

    handleClick(id) {
        this.setState({slide: id});
    }

    render() {

        return (
            <div className="carousel">
                <ul className="carousel__list" data-active-slide={ this.state.slide } ref={ this.carousel }>

                    {this.state.slideItems.map(function (slide, index, slideItems) {
                        return carouselItem(slide.title, slide.subtitle, slide.listItems, index, slideItems);
                    })}

                </ul>
                <div className="carousel__nav">
                    {this.state.slideItems.map((slide, index) => {
                        const id = (index + 1);
                        return (
                            <button className="carousel__nav-button" data-color-id={ id } onClick={ this.handleClick.bind(this, id) } key={ id }>
                                <span className="carousel__nav-button-text" dangerouslySetInnerHTML={{ __html: slide.navTitle ? slide.navTitle : slide.title }}></span>
                            </button>
                        );
                    })}
                </div>
            </div>
        );
    }
}

export default Carousel;