import React from 'react';

import './Hero.css';

function Hero (props) {
    const bannerPath = process.env.PUBLIC_URL + '/banner-list-items/';

    const HeroTitle = function () {
        return <h2 className="hero__title" dangerouslySetInnerHTML={{ __html: props.title }}></h2>;
    };

    const HeroSubtitle = function () {
        return props.subtitle
            ? <p className="hero__subtitle" dangerouslySetInnerHTML={{ __html: props.subtitle }}></p>
            : '';
    };

    const ListItems = function () {
        if (props.listItems.length) {
            return (
                <ul className="hero__list">
                    { props.listItems.map(function (listItem, index) {
                         return <li className="hero__list-item" dangerouslySetInnerHTML={{ __html: listItem }} key={ index }></li>;
                    }) }
                </ul>
            );
        }
    };

    const HeroImages = function () {
        if (!props.image.length) {
            return;
        }

        return (
            <div className="hero__images">
                <picture className="hero__image hero__image-first">
                    <source srcSet={bannerPath + '588w/a/' + props.image + '.png'} media="(min-width: 1300px)" />
                    <source srcSet={bannerPath + '452w/a/' + props.image + '.png'} media="(min-width: 1024px)" />
                    <source srcSet={bannerPath + '370w/a/' + props.image + '.png'} media="(min-width: 768px)" />
                    <img       src={bannerPath + '320w/a/' + props.image + '.png'} alt="" />
                </picture>
                <picture className="hero__image hero__image-second">
                    <source srcSet={bannerPath + '588w/b/' + props.image + '.svg'} media="(min-width: 1300px)" />
                    <source srcSet={bannerPath + '452w/b/' + props.image + '.svg'} media="(min-width: 1024px)" />
                    <source srcSet={bannerPath + '370w/b/' + props.image + '.svg'} media="(min-width: 768px)" />
                    <img       src={bannerPath + '320w/b/' + props.image + '.svg'} alt="" />
                </picture>
            </div>
        );
    }

    return (
        <div className="hero">
            <div className="hero__main">
                <HeroTitle/>
                <HeroSubtitle/>
                <ListItems/>
            </div>
            <HeroImages/>
            <a href="/" className="hero__button">Консультация</a>
        </div>
    );
}

export default Hero;